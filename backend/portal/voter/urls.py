from django.urls import path
from django.contrib import admin

from voter.views import VoterList

app_name = 'voter'

urlpatterns = [
    path('', VoterList.as_view(), name='voter_list_view'),
]