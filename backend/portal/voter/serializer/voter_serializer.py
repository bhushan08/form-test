from rest_framework import serializers
from voter.models import Voter

class VoterSerializer(serializers.ModelSerializer):

    class Meta:
        model = Voter
        fields = '__all__'

    def create(self, validated_data):
        return Voter.objects.create(**validated_data)