from django.db import models

# Create your models here.
class Voter(models.Model):
    first_name = models.CharField(max_length=32)
    last_name = models.CharField(max_length=32)
    mobile_number = models.IntegerField(default=0)
    email_address = models.EmailField()
    address = models.TextField(max_length=200)
    image = models.ImageField(blank=False, upload_to='images/', default=0)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.first_name +" "+ self.last_name
