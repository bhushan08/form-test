from django.shortcuts import render
from voter.models import Voter
from voter.serializer.voter_serializer import VoterSerializer
from rest_framework.generics import ListAPIView, ListCreateAPIView
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser, FormParser

# Create your views here.
class VoterList(APIView):
    parser_classes = (MultiPartParser, FormParser)

    def get(self, request):
        voter = Voter.objects.all()
        # the many param informs the serializer that it will be serializing more than a single voter.
        serializer = VoterSerializer(voter, many=True)
        return Response(serializer.data)

    def post(self, request):
        # Create an new Voter from the above data
        serializer = VoterSerializer(data=request.data)
        if serializer.is_valid(raise_exception=True):
            voter_saved = serializer.save()
        return Response({"success": "Voter '{}' added successfully".format(voter_saved.first_name)})