import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import CloudUploadIcon from "@material-ui/icons/CloudUpload";
import SaveIcon from "@material-ui/icons/Save";
import Button from "@material-ui/core/Button";
import "./registerForm.css";

const CssTextField = withStyles({
  root: {
    "& label.Mui-focused": {
      color: "#1A73E8"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#DADCE0"
      },
      "&:hover fieldset": {
        borderColor: "#1A73E8"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#1A73E8"
      }
    }
  }
})(TextField);

const inputStyle = {
  display: "none"
};

class RegisterForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      firstName: "",
      lastName: "",
      mobileNumber: "",
      email: "",
      address: "",
      image: null
    };
  }

  getVoterData = async () => {
    try {
      const res = await fetch("http://127.0.0.1:8000/voterlist/");
      const data = await res.json();
      this.setState({
        data: data
      });
      console.log(data);
    } catch (e) {
      console.log(e);
    }
  };

  componentDidMount() {
    this.getVoterData();
  }

  validate = () => {
    let emailError = "";
    if (/[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}/.test(this.state.email)) {
    } else {
      emailError = "Invalid email. Please enter a valid email.";
    }

    if (emailError) {
      this.setState({ emailError });
      return false;
    }

    return true;
  };

  handleImageChange = e => {
    this.setState({
      image: e.target.files[0]
    });
  };

  handleChange = e => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = e => {
    e.preventDefault();
    console.log(this.state);
    const form_data = new FormData();
    form_data.append("first_name", this.state.firstName);
    form_data.append("last_name", this.state.lastName);
    form_data.append("mobile_number", this.state.mobileNumber);
    form_data.append("email_address", this.state.email);
    form_data.append("address", this.state.address);
    form_data.append("image", this.state.image, this.state.image.name);
    fetch("http://127.0.0.1:8000/voterlist/", {
      method: "POST",
      body: form_data
      // headers: {
      //   "content-type": "multipart/form-data"
      // }
    })
      .then(res => {
        console.log(res);
      })
      .catch(err => console.log(err));
  };

  render() {
    const classes = withStyles();

    return (
      <div className="form-full">
        <div className="form-header">
          <h2>Registeration Form</h2>
        </div>
        <form className="form-body">
          <CssTextField
            className="form-input-textfield"
            id="firstName"
            label="First Name"
            variant="outlined"
            value={this.state.firstName.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="lastName"
            label="Last Name"
            variant="outlined"
            value={this.state.lastName.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="email"
            label="Email ID"
            variant="outlined"
            value={this.state.email.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="mobileNumber"
            label="Mobile Number"
            variant="outlined"
            value={this.state.mobileNumber.value}
            onChange={this.handleChange}
          />
          <CssTextField
            className="form-input-textfield"
            id="address"
            multiline
            rows="4"
            label="Address"
            variant="outlined"
            value={this.state.address.value}
            onChange={this.handleChange}
          />
          <input
            accept="image/*"
            className={classes.input}
            id="contained-button-file"
            multiple
            type="file"
            color="white"
            style={inputStyle}
            onChange={this.handleImageChange}
          />
          <label htmlFor="contained-button-file">
            <Button
              variant="contained"
              color="primary"
              component="span"
              startIcon={<CloudUploadIcon />}
            >
              Upload
            </Button>
          </label>
        </form>
        <div className="form-footer">
          <Button
            id="form-footer-button"
            variant="contained"
            color="primary"
            size="large"
            className={classes.button}
            startIcon={<SaveIcon />}
            onClick={this.handleSubmit}
          >
            Submit
          </Button>
        </div>
        {/* <div>
          {this.state.data.map(item => (
            <div key={item.id}>
              <h1>{item.first_name}</h1>
              <img
                style={{ heght: 200, width: 100 }}
                src={"http://localhost:8000" + item.image}
              ></img>
            </div>
          ))}
        </div>
        {console.log("RENDER RETURNED")} */}
      </div>
    );
  }
}

export default RegisterForm;
